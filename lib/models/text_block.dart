import 'package:blox/models/block.dart';
import 'package:blox/models/text_block_attr.dart';
import 'package:json_annotation/json_annotation.dart';

part 'text_block.g.dart';

/// TextBlock model.
@JsonSerializable()
class TextBlock extends Block {
  @JsonKey(defaultValue: -1)
  final int id;
  @JsonKey(defaultValue: BlockType.undefined)
  final BlockType type;
  @JsonKey(defaultValue: '')
  String text;
  final TextBlockAttr style;

  TextBlock(this.id, this.text, this.style, {this.type = BlockType.text})
      : super(id, BlockType.text);

  factory TextBlock.fromJson(Map<String, dynamic> json) =>
      _$TextBlockFromJson(json);

  Map<String, dynamic> toJson() => _$TextBlockToJson(this);
}
