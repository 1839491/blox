import 'package:json_annotation/json_annotation.dart';

part 'video_block_attr.g.dart';

/// VideoBlock Attributes (styles) model.
@JsonSerializable()
class VideoBlockAttr {
  @JsonKey(name: 'cover', required: false)
  String coverImageUrl;
  @JsonKey(name: 'duration', required: false)
  Duration duration;

  VideoBlockAttr();

  factory VideoBlockAttr.fromJson(Map<String, dynamic> json) =>
      _$VideoBlockAttrFromJson(json);

  Map<String, dynamic> toJson() => _$VideoBlockAttrToJson(this);
}
