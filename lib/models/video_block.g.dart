// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_block.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoBlock _$VideoBlockFromJson(Map<String, dynamic> json) {
  return VideoBlock(
    json['id'] as int ?? -1,
    json['video_url'] as String,
    json['style'] == null
        ? null
        : VideoBlockAttr.fromJson(json['style'] as Map<String, dynamic>),
    type: _$enumDecodeNullable(_$BlockTypeEnumMap, json['type']) ??
        BlockType.undefined,
  );
}

Map<String, dynamic> _$VideoBlockToJson(VideoBlock instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$BlockTypeEnumMap[instance.type],
      'video_url': instance.videoUrl,
      'style': instance.style?.toJson(),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$BlockTypeEnumMap = {
  BlockType.text: 1,
  BlockType.image: 2,
  BlockType.video: 3,
  BlockType.list: 4,
  BlockType.undefined: -1,
};
