## 0.0.7+8
* Fix some issue.
* New constants for BloxEditor (change the colors).
## 0.0.7+5

* Add bloxDecoration to BloxEditor (for change the icons).
## 0.0.7+4

* Fix some responsive errors.
## 0.0.7+3

* Change the dropdown ui.
* Fix some issues.
## 0.0.7, 0.0.7+1

* Fix Some errors.
## 0.0.6+9

* You can control the icons and the plugin message in BloxController().
## 0.0.6+8

* Fix Some errors.
## 0.0.6+7

* Now you can change the bloxEditor block's decoration.
* Some issues fixed.
## 0.0.6+1 & 0.0.6+2 & 0.0.6+3

* Fix some issue.
## 0.0.6

* Now you can change the bloxEditor block's border radius.
* Some bug fixed.
## 0.0.5

* Users can change the color of text and the font of them.
## 0.0.4

* Initial development release.

