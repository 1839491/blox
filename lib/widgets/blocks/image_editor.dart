import 'package:blox/models/image_block.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:universal_html/html.dart' as html;

class BloxImageEditor extends StatefulWidget {
  final ImageBlock imageBlock;
  final String defaultImageUrl;
  final String Function(String blob) onImageSelect;
  final Widget Function(Function() onTap, String message) child;
  final bool isBlob;

  const BloxImageEditor(
      {Key key,
      this.imageBlock,
      @required this.defaultImageUrl,
      @required this.onImageSelect,
      this.child,
      this.isBlob = false})
      : super(key: key);

  @override
  _BloxImageEditorState createState() => _BloxImageEditorState();
}

class _BloxImageEditorState extends State<BloxImageEditor> {
  String errorMsg = "";

  void onTap() async {
    _startFilePicker();
  }

  _startFilePicker() async {
    html.InputElement uploadInput = html.FileUploadInputElement();
    uploadInput.click();
    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final files = uploadInput.files;
      if (files.length == 1) {
        final file = files[0];
        html.FileReader reader = html.FileReader();
        reader.onLoadEnd.listen((e) {
          if (file.type == "image/png" ||
              file.type == "image/jpeg" ||
              file.type == "image/jpg") {
            setState(() {
              errorMsg = "";
              widget.imageBlock.imageUrl = widget.onImageSelect(reader.result);
            });
          } else {
            setState(() {
              errorMsg = "File type is wrong.";
            });
          }
        });

        reader.onError.listen((fileEvent) {
          setState(() {
            errorMsg = ("Some Error occured while reading the file");
          });
        });

        reader.readAsDataUrl(file);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.imageBlock.imageUrl.isEmpty ||
        widget.imageBlock == null ||
        widget.imageBlock.imageUrl == null) {
      return widget.child != null
          ? widget.child(onTap, errorMsg)
          : _buildButton(onTap);
    } else {
      return Column(children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            widget.imageBlock.imageUrl,
            height: 200,
          ),
        ),
        widget.child != null
            ? widget.child(onTap, errorMsg)
            : _buildButton(onTap, text: "Change image")
      ]);
    }
  }

  _buildButton(onTap, {text = "Chose image"}) {
    return RaisedButton(
      onPressed: onTap,
      child: Text(
        text + (errorMsg.isNotEmpty ? " (" + errorMsg + ") " : ""),
        style: TextStyle(color: Colors.black),
      ),
      color: Colors.amber,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
    );
  }
}
