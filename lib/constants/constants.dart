import 'package:flutter/material.dart';

class Constants {
  static const String DEFAULT_FONT_FAMILY = 'Yekan';
  static const double DEFAULT_FONT_SIZE = 16;
  static const BoxDecoration DEFAULT_DECORATION = BoxDecoration(
    color: Colors.white,
    boxShadow: [
      BoxShadow(color: Colors.black12, blurRadius: 10, spreadRadius: 0.5)
    ],
    borderRadius: BorderRadius.all(Radius.circular(8)),
  );
}
