import 'package:blox/models/image_block.dart';
import 'package:blox/models/text_block.dart';
import 'package:blox/models/video_block.dart';

/// BloxDocument model to parse and generate json
class BloxDocument {
  final List<dynamic> blocks;

  BloxDocument(this.blocks);

  factory BloxDocument.fromJson(Map<String, dynamic> json) {
    var items = json['blocks'] as List;
    var newItems = items.map((e) {
      if (e['type'] == 1) {
        return TextBlock.fromJson(e);
      }
      if (e['type'] == 2) {
        return ImageBlock.fromJson(e);
      }
      if (e['type'] == 3) {
        return VideoBlock.fromJson(e);
      }
      return null;
    }).toList();
    return BloxDocument(newItems);
  }

  Map<String, dynamic> toJson() {
    final items = List<Map<String, dynamic>>();
    for (var b in blocks) {
      items.add(b.toJson());
    }
    return {'blocks': items};
  }
}
