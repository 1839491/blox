import 'package:blox/constants/package_config.dart';
import 'package:blox/widgets/blocks/text_editor.dart';
import 'package:flutter/material.dart';

class BloxFonts {
  /// Key(Font Family Name): Value(Font name for display)
  Map<String, dynamic> fontsMap = {
    "Robot": "Robot",
    "RaleWay": "RaleWay",
    "BebasNeue": "BebasNeue",
    "Monda": "Monda",
    "DiroozEn": "Dirooz",
    "Anton": "Anton",
    "Sahel": "ساحل",
    "IranSans": "ایران سنس",
    "Lalezar": "لاله زار",
    "Titr": "تیتر",
    "Yekan": "یکان",
    "Vazir": "وزیر",
    "Khodkar": "خودکار",
    "Mitra": "میترا",
    "Ghasem": "قاسم",
    "Nastaliq": "نستعلیق",
    "Parastoo": "پرستو",
    "DiroozFa": "دیروز",
  };

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = List<DropdownMenuItem<String>>();
    fontsMap.forEach((key, value) {
      items.add(DropdownMenuItem(
          value: key,
          child: Directionality(
            textDirection: isRTL(value) ? TextDirection.rtl : TextDirection.ltr,
            child: Row(
              children: [
                Text(
                  value,
                  textDirection:
                      isRTL(value) ? TextDirection.rtl : TextDirection.ltr,
                  textAlign: isRTL(value) ? TextAlign.right : TextAlign.left,
                  softWrap: true,
                  style: TextStyle(
                      fontFamily: key, package: PackageConfig.PACKAGE_NAME),
                ),
              ],
            ),
          )));
    });
    return items;
  }
}
