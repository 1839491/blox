#import "BloxPlugin.h"
#if __has_include(<blox/blox-Swift.h>)
#import <blox/blox-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "blox-Swift.h"
#endif

@implementation BloxPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBloxPlugin registerWithRegistrar:registrar];
}
@end
