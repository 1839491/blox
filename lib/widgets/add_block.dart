import 'package:animate_do/animate_do.dart';
import 'package:blox/constants/package_config.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AddBlockWidget extends StatefulWidget {
  /// list of items you want to display on AddBlockButtonClick
  final List<AddBlockItem> children;

  /// if you want this open on hover in addBlockButton you should make it true
  final bool openOnHover;

  ///
  final List<AddBlockItem> Function(
      bool Function(bool state) display, List<AddBlockItem> children) builder;

  /// Show Items horizontally or vertically
  final Axis axis;

  /// Constants of AddBlockWidget
  const AddBlockWidget(
      {Key key,
      this.children,
      this.openOnHover = false,
      @required this.builder,
      this.axis = Axis.vertical})
      : super(key: key);

  @override
  _AddBlockWidgetState createState() => _AddBlockWidgetState();
}

class _AddBlockWidgetState extends State<AddBlockWidget> {
  /// AddBlock Items state (open=>true or close=>false)
  bool _isOpen = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onExit: (e) {
        if (widget.openOnHover) {
          setState(() {
            _isOpen = false;
          });
        }
      },
      child: widget.axis == Axis.horizontal
          ? Row(mainAxisSize: MainAxisSize.min, children: _buildElements())
          : Column(
              mainAxisSize: MainAxisSize.min,
              children: _buildElements(),
            ),
    );
  }

  animationFromAxis({Widget child}) {
    return widget.axis == Axis.horizontal
        ? BounceInLeft(
            child: child,
          )
        : BounceInDown(
            child: child,
          );
  }

  _buildElements() {
    return [
      MouseRegion(
        onHover: (e) {
          if (widget.openOnHover) {
            setState(() {
              _isOpen = true;
            });
          }
        },
        child: GestureDetector(
          onTap: () {
            if (!widget.openOnHover)
              setState(() {
                if (_isOpen) {
                  _isOpen = false;
                } else {
                  _isOpen = true;
                }
              });
          },
          child: Container(
              width: 45,
              height: 45,
              child: FlareActor(
                PackageConfig.PACKAGE_ADDRESS + "assets/flare/addButton.flr",
                animation: _isOpen.toString(),
              )),
        ),
      ),
      SizedBox(
        width: 5,
        height: 5,
      ),
      Visibility(
        visible: _isOpen,
        child: animationFromAxis(
          child: Container(
            padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, blurRadius: 10, spreadRadius: 0.5)
                ]),
            child: widget.axis == Axis.horizontal
                ? Row(mainAxisSize: MainAxisSize.min, children: _buildItems())
                : Column(
                    mainAxisSize: MainAxisSize.min,
                    children: _buildItems(),
                  ),
          ),
        ),
      )
    ];
  }

  _buildItems() {
    return [
      SizedBox(width: 2),
      // ignore: missing_return
      ...widget.builder((state) {
            if (state == false)
              setState(() {
                _isOpen = state;
              });
          }, widget.children) ??
          widget.children,
      SizedBox(width: 2),
    ];
  }
}

class AddBlockItem extends StatelessWidget {
  /// item on tap action.
  final VoidCallback onTap;

  /// item icon.
  final IconData iconData;

  /// item tint color.
  final Color iconColor;

  /// iconSize
  final double iconSize;

  /// item tooltip text or some hint for tell what this item do.
  final String tooltipText;

  final EdgeInsets padding;
  const AddBlockItem(
      {Key key,
      this.onTap,
      @required this.iconData,
      this.iconColor = Colors.black,
      this.tooltipText,
      this.iconSize,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return tooltipText != null
        ? Tooltip(
            message: tooltipText ?? '',
            child: _buildWidget,
          )
        : _buildWidget;
  }

  Widget get _buildWidget {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          if (onTap != null) onTap();
        },
        borderRadius: BorderRadius.circular(25),
        child: Padding(
          padding: padding ?? EdgeInsets.all(8),
          child: Icon(
            iconData,
            size: iconSize,
            color: iconColor,
          ),
        ),
      ),
    );
  }
}
