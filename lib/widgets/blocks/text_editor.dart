import 'package:blox/blox.dart';
import 'package:blox/constants/constants.dart';
import 'package:blox/constants/package_config.dart';
import 'package:blox/constants/supporting_colors.dart';
import 'package:blox/constants/supporting_fonts.dart';
import 'package:blox/models/color_block.dart';
import 'package:blox/models/text_block_attr.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart' as intl;

import '../adaptive.dart';
import '../add_block.dart';

class BloxTextEditor extends StatefulWidget {
  final String text;
  final double textSize;
  final String fontFamily;
  final BlockColor color;
  final TextBlockAlign align;
  final String hint;
  final Function(String value, double fontSize, String fontFamily,
      BlockColor color, TextBlockAlign align) onChange;
  final bool multiLine;
  final String colorPickerHintLang;
  final BloxDecoration bloxDecoration;

  const BloxTextEditor(
      {Key key,
      this.text,
      this.onChange,
      this.textSize = 16,
      this.multiLine,
      this.fontFamily = Constants.DEFAULT_FONT_FAMILY,
      this.color,
      this.align = TextBlockAlign.auto,
      this.colorPickerHintLang = "FA",
      this.hint = "",
      this.bloxDecoration})
      : super(key: key);

  @override
  _BloxTextEditorState createState() => _BloxTextEditorState();
}

class _BloxTextEditorState extends State<BloxTextEditor> {
  TextEditingController _textEditingController = TextEditingController();
  int lines = 1;
  String vlue = "";
  double textSizeLocale;
  String fontFamilyName;
  BlockColor color;
  TextBlockAlign align;

  @override
  void initState() {
    super.initState();
    if (widget.multiLine)
      lines = '\n'.allMatches(widget.text).length == 0
          ? 2
          : '\n'.allMatches(widget.text).length + 1;
    _textEditingController.text = widget.text;
    vlue = widget.text;
    textSizeLocale = widget.textSize;
    fontFamilyName = widget.fontFamily;
    color = widget.color;
    align = widget.align;
  }

  @override
  void dispose() {
    super.dispose();
    _textEditingController.dispose();
  }

  List<Widget> _buildEditorTools() {
    return [
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          AddBlockItem(
            onTap: () {
              setState(() {
                if (textSizeLocale >= 18) textSizeLocale -= 2;
              });
              onChangeNotifier();
            },
            iconData: widget.bloxDecoration.textSizeIcon,
            iconColor: widget.bloxDecoration.textSizeIconColor,
            iconSize: 16,
            padding: EdgeInsets.all(11),
          ),
          SizedBox(
            width: 5,
          ),
          AddBlockItem(
            onTap: () {
              setState(() {
                if (textSizeLocale <= 40) textSizeLocale += 2;
              });
              onChangeNotifier();
            },
            iconData: widget.bloxDecoration.textSizeIcon,
            iconColor: widget.bloxDecoration.textSizeIconColor,
            iconSize: 20,
          ),
          SizedBox(
            width: 5,
          ),
          MouseRegion(
            cursor: SystemMouseCursors.click,
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: DropdownButton(
                underline: SizedBox.shrink(),
                value: fontFamilyName,
                items: BloxFonts().getDropDownMenuItems(),
                icon: Icon(widget.bloxDecoration.fontsDropdownMenuIcon,
                    color: widget.bloxDecoration.dropdownIconColor),
                onChanged: (String value) {
                  setState(() {
                    fontFamilyName = value;
                  });
                  onChangeNotifier();
                },
              ),
            ),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          MouseRegion(
            cursor: SystemMouseCursors.click,
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: DropdownButton(
                icon: widget.bloxDecoration.colorsDropdownMenuIcon,
                underline: SizedBox.shrink(),
                value: BloxColors().colorToHex(Color.fromARGB(
                    color.alpha, color.red, color.green, color.blue)),
                items: BloxColors()
                    .getDropDownMenuItems(hintLang: widget.colorPickerHintLang),
                onChanged: (String value) {
                  setState(() {
                    Color clr = BloxColors().hexToColor(value);
                    color = BlockColor(clr.red, clr.green, clr.blue, clr.alpha);
                  });
                  onChangeNotifier();
                },
              ),
            ),
          ),
          AddBlockItem(
            iconData: widget.bloxDecoration.alignLeftIcon,
            iconColor: align == TextBlockAlign.left
                ? widget.bloxDecoration.alignActiveColor
                : widget.bloxDecoration.alignDisabledColor,
            onTap: () {
              setState(() {
                if (align == TextBlockAlign.left)
                  align = TextBlockAlign.auto;
                else
                  align = TextBlockAlign.left;
              });

              onChangeNotifier();
            },
          ),
          AddBlockItem(
            iconData: widget.bloxDecoration.alignCenterIcon,
            iconColor: align == TextBlockAlign.center
                ? widget.bloxDecoration.alignActiveColor
                : widget.bloxDecoration.alignDisabledColor,
            onTap: () {
              setState(() {
                if (align == TextBlockAlign.center)
                  align = TextBlockAlign.auto;
                else
                  align = TextBlockAlign.center;
              });
              onChangeNotifier();
            },
          ),
          AddBlockItem(
            iconData: widget.bloxDecoration.alignRightIcon,
            iconColor: align == TextBlockAlign.right
                ? widget.bloxDecoration.alignActiveColor
                : widget.bloxDecoration.alignDisabledColor,
            onTap: () {
              setState(() {
                if (align == TextBlockAlign.right)
                  align = TextBlockAlign.auto;
                else
                  align = TextBlockAlign.right;
              });
              onChangeNotifier();
            },
          ),
        ],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (!isDisplayDesktop(context)) ...[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: _buildEditorTools().reversed.toList(),
          ),
        ] else ...[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: _buildEditorTools(),
          ),
        ],
        TextFormField(
          textDirection: isRTL(vlue) ? TextDirection.rtl : TextDirection.ltr,
          textAlign: getTextAlign(),
          enableSuggestions: true,
          cursorColor: Colors.red,
          cursorWidth: 1,
          showCursor: true,
          maxLines: lines,
          style: TextStyle(
              color: Color.fromARGB(
                  color.alpha, color.red, color.green, color.blue),
              fontFamily: fontFamilyName,
              package: PackageConfig.PACKAGE_NAME,
              fontSize: textSizeLocale),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.hint,
            hintMaxLines: 1,
          ),
          controller: _textEditingController,
          onChanged: (value) {
//            _textEditingController.selection = TextSelection.fromPosition(TextPosition(offset:_textEditingController.text.length));
            if (widget.multiLine)
              lines = '\n'.allMatches(value).length == 0
                  ? 2
                  : '\n'.allMatches(value).length + 1;
            setState(() {
              vlue = value;
            });
            onChangeNotifier();
          },
        ),
      ],
    );
  }

  getTextAlign() {
    switch (align) {
      case TextBlockAlign.auto:
        return isRTL(vlue) ? TextAlign.right : TextAlign.left;
        break;
      case TextBlockAlign.center:
        return TextAlign.center;
        break;
      case TextBlockAlign.left:
        return TextAlign.left;
        break;
      case TextBlockAlign.right:
        return TextAlign.right;
        break;
    }
  }

  onChangeNotifier() {
    widget.onChange(vlue, textSizeLocale, fontFamilyName, color, align);
  }
}

bool isRTL(String text) {
  return intl.Bidi.detectRtlDirectionality(text);
}
