import 'dart:convert' show json;
import 'dart:io';

import 'package:blox/models/block.dart';
import 'package:blox/models/color_block.dart';
import 'package:blox/models/image_block.dart';
import 'package:blox/models/list_block.dart';
import 'package:blox/models/text_block.dart';
import 'package:blox/models/text_block_attr.dart';
import 'package:blox/models/video_block.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Testing Models', () {
    test('parse TextBlock model', () async {
      final file = File('test_resources/text_block_model.json');
      final jsonString = json.decode(await file.readAsString());

      final textBlock = TextBlock.fromJson(jsonString);
      expect(textBlock.id, 1);
      expect(textBlock.text, 'Hello');
      expect(textBlock.type, BlockType.text);
      // Testing styles
      expect(textBlock.style.fontSize, 10);
      expect(textBlock.style.fontFamily, 'myFont');
      expect(textBlock.style.multiline, true);
      // Testing color
      expect(textBlock.style.color.red, 0);
      expect(textBlock.style.color.green, 0);
      expect(textBlock.style.color.blue, 0);
      expect(textBlock.style.color.alpha, 255);
    });

    test('parse ImageBlock model', () async {
      final file = File('test_resources/image_block_model.json');
      final jsonString = json.decode(await file.readAsString());

      final imageBlock = ImageBlock.fromJson(jsonString);
      expect(imageBlock.id, 1);
      expect(imageBlock.imageUrl, 'https://google.com/image.png');
      expect(imageBlock.type, BlockType.image);
      expect(
          imageBlock.style.placeholder, 'https://google.com/placeholder.png');
      expect(imageBlock.style.altText, 'something cool');
      expect(imageBlock.style.hyperlink, 'https://apexteam.net');
    });

    test('parse VideoBlock model', () async {
      final file = File('test_resources/video_block_model.json');
      final jsonString = json.decode(await file.readAsString());

      final videoBlock = VideoBlock.fromJson(jsonString);
      expect(videoBlock.id, 1);
      expect(videoBlock.type, BlockType.video);
      expect(videoBlock.videoUrl, 'https://google.com/video.mp4');
      expect(videoBlock.style.coverImageUrl, 'https://google.com/cover.png');
      expect(videoBlock.style.duration, Duration(microseconds: 10));
    });

    test('parse ListBlock model', () async {
      final file = File('test_resources/list_block_model.json');
      final jsonString = json.decode(await file.readAsString());

      final listBlock = ListBlock.fromJson(jsonString);
      expect(listBlock.id, 1);
      expect(listBlock.type, BlockType.list);
      expect(listBlock.items.length, 3);
      expect(listBlock.items[0], 'Item1');
    });

    test('toJson BlockColor', () {
      final blockColor = BlockColor.black();
      final blockColorToJson = blockColor.toJson();
      final blockColorFromJson = BlockColor.fromJson(blockColorToJson);
      expect(blockColor.red, blockColorFromJson.red);
      expect(blockColor.blue, blockColorFromJson.blue);
      expect(blockColor.green, blockColorFromJson.green);
      expect(blockColor.alpha, blockColorFromJson.alpha);
    });

    test('parse TextBlock color field toJson method', () {
      final blockColor = BlockColor.white();
      final textBlock = TextBlock(1, 'test',
          TextBlockAttr('font', 10, false, color: BlockColor.white()));
      final textBlockToJson = textBlock.toJson();
      final textBlockFromJson = TextBlock.fromJson(textBlockToJson);
      expect(textBlockFromJson.style.color.red, blockColor.red);
      expect(textBlockFromJson.style.color.blue, blockColor.blue);
      expect(textBlockFromJson.style.color.green, blockColor.green);
      expect(textBlockFromJson.style.color.alpha, blockColor.alpha);
    });
  });
}
