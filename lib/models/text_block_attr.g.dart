// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'text_block_attr.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TextBlockAttr _$TextBlockAttrFromJson(Map<String, dynamic> json) {
  return TextBlockAttr(
    json['font_family'] as String ?? 'Yekan',
    (json['font_size'] as num)?.toDouble() ?? 16.0,
    json['multiline'] as bool ?? false,
    align: _$enumDecodeNullable(_$TextBlockAlignEnumMap, json['align']) ??
        TextBlockAlign.auto,
    color: json['color'] == null
        ? null
        : BlockColor.fromJson(json['color'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TextBlockAttrToJson(TextBlockAttr instance) =>
    <String, dynamic>{
      'font_family': instance.fontFamily,
      'font_size': instance.fontSize,
      'multiline': instance.multiline,
      'align': _$TextBlockAlignEnumMap[instance.align],
      'color': _colorToJson(instance.color),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$TextBlockAlignEnumMap = {
  TextBlockAlign.auto: -1,
  TextBlockAlign.center: 0,
  TextBlockAlign.left: 1,
  TextBlockAlign.right: 2,
};
