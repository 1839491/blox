import 'package:blox/constants/package_config.dart';
import 'package:blox/models/block.dart';
import 'package:blox/models/block_document.dart';
import 'package:blox/models/image_block.dart';
import 'package:blox/models/text_block.dart';
import 'blocks/image_view.dart';
import 'blocks/text_view.dart';
import 'package:flutter/material.dart';

class BloxView extends StatefulWidget {
  /// Blox Document (The document created from BloxEditor)
  final BloxDocument bloxDocument;

//  /// The Font Name for the persian texts from supporting_fonts.dart [BloxFonts]
//  final String persianFont;
//
//  /// The Font Name for the english texts from supporting_fonts.dart [BloxFonts]
//  final String englishFont;

  /// Image On Tap Action
  final Function(Block block) onBlockTap;

  const BloxView({Key key, @required this.bloxDocument, this.onBlockTap})
      : super(key: key);

  @override
  _BloxViewState createState() => _BloxViewState();
}

class _BloxViewState extends State<BloxView> {
  List<Widget> _list;

  @override
  void initState() {
    super.initState();
    setState(() {
      _list = widget.bloxDocument.blocks.map((e) {
        if (e is TextBlock) {
          return GestureDetector(
            child: BloxTextView(
              onTap: widget.onBlockTap,
              textBlock: e,
              style: TextStyle(
                  fontFamily: e.style.fontFamily,
                  package: PackageConfig.PACKAGE_NAME,
                  fontSize: e.style.fontSize,
                  color: Color.fromARGB(e.style.color.alpha, e.style.color.red,
                      e.style.color.green, e.style.color.blue)),
            ),
          );
        }
        if (e is ImageBlock) {
          return BloxImageView(
            onTap: widget.onBlockTap,
            imageBlock: e,
          );
        }

        /// Add More Blocks Here
        return null;
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: [..._list],
    );
  }
}
