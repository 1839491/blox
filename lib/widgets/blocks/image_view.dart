import 'package:blox/models/block.dart';
import 'package:blox/models/image_block.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class BloxImageView extends StatefulWidget {
  final ImageBlock imageBlock;
  final Function(Block e) onTap;

  const BloxImageView({Key key, this.onTap, this.imageBlock}) : super(key: key);

  @override
  _BloxImageViewState createState() => _BloxImageViewState();
}

class _BloxImageViewState extends State<BloxImageView> {
  String heroTag;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      MouseRegion(
        child: GestureDetector(
          onTap: () {
            if (widget.onTap != null) widget.onTap(widget.imageBlock);
          },
          child: Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, blurRadius: 10, spreadRadius: 0.05)
                ]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.network(
                widget.imageBlock.imageUrl,
                height: MediaQuery.of(context).size.width > 400 ? 260 : 100,
              ),
            ),
          ),
        ),
      ),
      SizedBox(
        height: 16,
      ),
    ]);
  }
}
