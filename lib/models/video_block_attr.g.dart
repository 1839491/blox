// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_block_attr.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoBlockAttr _$VideoBlockAttrFromJson(Map<String, dynamic> json) {
  return VideoBlockAttr()
    ..coverImageUrl = json['cover'] as String
    ..duration = json['duration'] == null
        ? null
        : Duration(microseconds: json['duration'] as int);
}

Map<String, dynamic> _$VideoBlockAttrToJson(VideoBlockAttr instance) =>
    <String, dynamic>{
      'cover': instance.coverImageUrl,
      'duration': instance.duration?.inMicroseconds,
    };
