import 'package:blox/models/block.dart';
import 'package:blox/models/text_block.dart';
import 'package:blox/models/text_block_attr.dart';
import 'package:blox/widgets/blocks/text_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class BloxTextView extends StatelessWidget {
  final TextBlock textBlock;
  final TextStyle style;
  final Function(Block block) onTap;

  const BloxTextView({
    Key key,
    this.textBlock,
    this.onTap,
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
//      cursor:
//          onTap != null ? SystemMouseCursors.click : SystemMouseCursors.basic,
      child: GestureDetector(
        onTap: () {
          if (onTap != null) onTap(textBlock);
        },
        child: Container(
          padding: EdgeInsets.all(8),
          child: Text(
            textBlock.text,
            textDirection:
                isRTL(textBlock.text) ? TextDirection.rtl : TextDirection.ltr,
            textAlign: getTextAlign(textBlock.style.align, textBlock.text),
            style: style,
          ),
        ),
      ),
    );
  }

  getTextAlign(align, text) {
    switch (align) {
      case TextBlockAlign.auto:
        return isRTL(text) ? TextAlign.right : TextAlign.left;
        break;
      case TextBlockAlign.center:
        return TextAlign.center;
        break;
      case TextBlockAlign.left:
        return TextAlign.left;
        break;
      case TextBlockAlign.right:
        return TextAlign.right;
        break;
    }
  }
}
