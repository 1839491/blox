import 'package:json_annotation/json_annotation.dart';

part 'image_block_attr.g.dart';

/// ImageBlock Attributes (styles) model.
@JsonSerializable()
class ImageBlockAttr {
  @JsonKey(required: false)
  String placeholder;
  @JsonKey(name: 'alt', required: false)
  String altText;
  @JsonKey(required: false)
  String hyperlink;

  ImageBlockAttr(this.placeholder, this.altText, this.hyperlink);

  factory ImageBlockAttr.fromJson(Map<String, dynamic> json) =>
      _$ImageBlockAttrFromJson(json);

  Map<String, dynamic> toJson() => _$ImageBlockAttrToJson(this);
}
