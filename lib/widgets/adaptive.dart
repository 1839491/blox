// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

enum DisplayType { desktop, mobile, tablet }

const _desktopPortraitBreakpoint = 700.0;
const _desktopLandscapeBreakpoint = 1000.0;

/// Returns the [DisplayType] for the current screen. This app only supports
/// mobile and desktop layouts, and as such we only have one breakpoint.
DisplayType displayTypeOf(BuildContext context) {
  final orientation = MediaQuery.of(context).orientation;
  final width = MediaQuery.of(context).size.width;
  final height = MediaQuery.of(context).size.height;

//  if ((orientation == Orientation.landscape &&
//          width > _desktopLandscapeBreakpoint) ||
//      (orientation == Orientation.portrait &&
//          width > _desktopPortraitBreakpoint)) {
//    return DisplayType.desktop;
//  } else {
//    return DisplayType.mobile;

  double deviceWidth = 0;
  if (orientation == Orientation.landscape) {
    deviceWidth = height;
  } else {
    deviceWidth = width;
  }

  if (deviceWidth > 950 ||
      ((orientation == Orientation.landscape &&
              width > _desktopLandscapeBreakpoint) ||
          (orientation == Orientation.portrait &&
              width > _desktopPortraitBreakpoint))) {
    return DisplayType.desktop;
  }

  if (deviceWidth > 600) {
    return DisplayType.tablet;
  }

  return DisplayType.mobile;
}

/// Returns a boolean if we are in a display of [DisplayType.desktop]. Used to
/// build adaptive and responsive layouts.
bool isDisplayDesktop(BuildContext context) {
  return displayTypeOf(context) == DisplayType.desktop;
}

/// Returns a boolean if we are in a display of [DisplayType.desktop] or [DisplayType.tablet]. Used to
/// build adaptive and responsive layouts.
bool isDisplayDesktopOrTablet(BuildContext context) {
  return displayTypeOf(context) == DisplayType.desktop ||
      displayTypeOf(context) == DisplayType.tablet;
}

/// Returns a boolean if we are in a display of [DisplayType.desktop] but less
/// than [_desktopLandscapeBreakpoint] width. Used to build adaptive and responsive layouts.
bool isDisplaySmallDesktop(BuildContext context) {
  return isDisplayDesktop(context) &&
      MediaQuery.of(context).size.width < _desktopLandscapeBreakpoint;
}

class ScreenInformation {
  final Size screenSize;
  final Size localWidgetSize;
  final DisplayType deviceType;
  final Orientation orientation;

  ScreenInformation(
      {this.screenSize,
      this.localWidgetSize,
      this.deviceType,
      this.orientation});
}

DisplayType getDeviceType(MediaQueryData media) {
  final orientation = media.orientation;

  double deviceWidth = 0;
  if (orientation == Orientation.landscape) {
    deviceWidth = media.size.height;
  } else {
    deviceWidth = media.size.width;
  }

  if (deviceWidth > 950) {
    return DisplayType.desktop;
  }

  if (deviceWidth > 600) {
    return DisplayType.tablet;
  }

  return DisplayType.mobile;
}
