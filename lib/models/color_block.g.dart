// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'color_block.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BlockColor _$BlockColorFromJson(Map<String, dynamic> json) {
  return BlockColor(
    json['red'] as int,
    json['green'] as int,
    json['blue'] as int,
    json['alpha'] as int,
  );
}

Map<String, dynamic> _$BlockColorToJson(BlockColor instance) =>
    <String, dynamic>{
      'red': instance.red,
      'green': instance.green,
      'blue': instance.blue,
      'alpha': instance.alpha,
    };
