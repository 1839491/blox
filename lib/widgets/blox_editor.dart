import 'package:blox/blox.dart';
import 'package:blox/constants/constants.dart';
import 'package:blox/constants/package_config.dart';
import 'package:blox/models/block.dart';
import 'package:blox/models/color_block.dart';
import 'package:blox/models/image_block.dart';
import 'package:blox/models/image_block_attr.dart';
import 'package:blox/models/text_block.dart';
import 'package:blox/models/text_block_attr.dart';
import 'package:blox/widgets/add_block.dart';
import 'package:blox/widgets/blocks/text_editor.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mbi/combi_icons.dart';
import 'package:mbi/linecons_icons.dart';
import 'package:mbi/mbi.dart';

class BloxEditor extends StatefulWidget {
  /// If the Mouse come in hover of the addblock button it will be open if you set this parameter as true
  final bool addButtonOpenOnHover;

  /// The Write can't delete block if you set it false.
  final bool canDeleteBlocks;

  /// use the BloxImageEditor for this variable to manage the pictures in editor
  final BloxImageEditor Function(ImageBlock imageBlock) imageEditor;

  /// Control the BloxEditor, get the result with [BloxController]
  final BloxController controller;

  /// Blox Icons and text messages.
  final BloxDecoration bloxDecoration;

  /// Equal FA/EN set the color picker hints (آبی/Blue)
  final String colorPickerHintLang;

  /// The Border radius of blocks
  final BoxDecoration blockDecoration;

  /// Show add button when mouse come in the block hover.
  final bool showAddButtonOnHover;

  const BloxEditor({
    Key key,
    this.addButtonOpenOnHover = false,
    this.canDeleteBlocks = true,
    @required this.imageEditor,
    @required this.controller,
    this.colorPickerHintLang = "FA",
    this.blockDecoration = Constants.DEFAULT_DECORATION,
    this.showAddButtonOnHover = true,
    this.bloxDecoration = const BloxDecoration(),
  }) : super(key: key);

  @override
  _BloxEditorState createState() => _BloxEditorState();
}

class _BloxEditorState extends State<BloxEditor> {
  int _addButtonHoveredIndex = -1;
  List<Map<String, dynamic>> finaleResult = [];

  @override
  void initState() {
    super.initState();
  }

  SystemMouseCursor cursor = SystemMouseCursors.basic;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: cursor,
      child: ListView(
        padding: EdgeInsets.all(8),
        children: <Widget>[
          for (int i = 0; i < widget.controller.bloxItems.length; i++) ...[
            /// TextBlock Editor Section
            if (widget.controller.bloxItems[i] is TextBlock) ...[
              _buildListItem(
                  i,
                  BloxTextEditor(
                    bloxDecoration: widget.bloxDecoration,
                    hint: widget.bloxDecoration.fieldsHint,
                    colorPickerHintLang: widget.colorPickerHintLang,
                    textSize: (widget.controller.bloxItems[i] as TextBlock)
                        .style
                        .fontSize,
                    fontFamily: (widget.controller.bloxItems[i] as TextBlock)
                        .style
                        .fontFamily,
                    align: (widget.controller.bloxItems[i] as TextBlock)
                        .style
                        .align,
                    color: (widget.controller.bloxItems[i] as TextBlock)
                        .style
                        .color,
                    multiLine: (widget.controller.bloxItems[i] as TextBlock)
                        .style
                        .multiline,
                    text: (widget.controller.bloxItems[i] as TextBlock).text,
                    onChange: (value, fontSize, fontFamily, color, align) {
                      (widget.controller.bloxItems[i] as TextBlock).text =
                          value;
                      (widget.controller.bloxItems[i] as TextBlock)
                          .style
                          .fontSize = fontSize;
                      (widget.controller.bloxItems[i] as TextBlock)
                          .style
                          .fontFamily = fontFamily;
                      (widget.controller.bloxItems[i] as TextBlock)
                          .style
                          .color = color;
                      (widget.controller.bloxItems[i] as TextBlock)
                          .style
                          .align = align;
                    },
                  ))
            ],

            /// ImageBlock Editor Section
            if (widget.controller.bloxItems[i] is ImageBlock) ...[
              _buildListItem(
                  i, widget.imageEditor(widget.controller.bloxItems[i])),
            ],

            /// Add More Blocks Here
          ],
          if (widget.controller.bloxItems.length == 0) ...[
            Center(
              child: Container(
                width: 400,
                height: 400,
                key: ValueKey("SingleAnimation"),
                child: FlareActor(
                  PackageConfig.PACKAGE_ADDRESS +
                      "assets/flare/addSomeBlock.flr",
                  animation: "animation",
                ),
              ),
            ),
            Center(
              child: widget.bloxDecoration.noneBloxWidget ??
                  Text(
                    "Add some block !",
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: Constants.DEFAULT_FONT_FAMILY,
                      package: PackageConfig.PACKAGE_NAME,
                      fontSize: 30,
                    ),
                  ),
            ),
            Center(
              child: SizedBox(
                key: ValueKey("SingleSizedBox"),
                height: 20,
              ),
            ),
            Center(
              child: _buildAddButton(-1,
                  key: ValueKey("SingleAddButton"), axis: Axis.horizontal),
            )
          ]
        ],
      ),
    );
  }

  /// Build Every Draggable list item
  _buildListItem(int i, Widget child) {
    return Column(
      key: ValueKey(widget.controller.bloxItems[i]),
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        MouseRegion(
          onHover: (e) {
            if (_addButtonHoveredIndex != i)
              setState(() {
                _addButtonHoveredIndex = i;
              });
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (i - 1 >= 0)
                    MouseRegion(
                      cursor: cursor,
                      child: AddBlockItem(
                        onTap: () {
                          _moveElement(oldIndex: i, newIndex: i - 1);
                        },
                        iconData: widget.bloxDecoration.chevronUpIcon,
                        iconColor: widget.bloxDecoration.chevronIconsColor,
                        iconSize: 30,
                      ),
                    ),
                  if (i + 1 <= widget.controller.bloxItems.length - 1)
                    MouseRegion(
                      cursor: cursor,
                      child: AddBlockItem(
                        onTap: () {
                          _moveElement(oldIndex: i, newIndex: i + 1);
                        },
                        iconData: widget.bloxDecoration.chevronDownIcon,
                        iconColor: widget.bloxDecoration.chevronIconsColor,
                        iconSize: 30,
                      ),
                    ),
                  if (widget.canDeleteBlocks)
                    AddBlockItem(
                      iconData: widget.bloxDecoration.deleteIcon,
                      iconColor: widget.bloxDecoration.deleteIconColor,
                      iconSize: 30,
                      onTap: () {
                        setState(() {
                          widget.controller.bloxItems.removeAt(i);
                        });
                      },
                    ),
                  if (_addButtonHoveredIndex == i ||
                      widget.showAddButtonOnHover)
                    MouseRegion(
                        onExit: (e) {
                          setState(() {
                            _addButtonHoveredIndex = -1;
                          });
                        },
                        child: _buildAddButton(i)),
                ],
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: widget.blockDecoration,
                  child: child,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  /// Add Section Button Options
  _buildAddButton(int index, {Key key, Axis axis = Axis.vertical}) {
    return AddBlockWidget(
      key: key,
      axis: axis,
      openOnHover: widget.addButtonOpenOnHover,
      builder: (display, children) {
        return [
          /// Title
          AddBlockItem(
            onTap: () {
              setState(() {
                display(false);
                widget.controller.bloxItems.insert(
                    index + 1,
                    TextBlock(
                        index + 1,
                        "",
                        TextBlockAttr(Constants.DEFAULT_FONT_FAMILY, 30, false,
                            color: BlockColor.black(),
                            align: TextBlockAlign.auto)));
              });
            },
            iconData: widget.bloxDecoration.titleIcon,
            iconColor: widget.bloxDecoration.titleIconColor,
            iconSize: 18,
          ),

          /// Paragraph
          AddBlockItem(
            onTap: () {
              setState(() {
                display(false);
                widget.controller.bloxItems.insert(
                    index + 1,
                    TextBlock(
                        index + 1,
                        "",
                        TextBlockAttr(Constants.DEFAULT_FONT_FAMILY, 16, true,
                            color: BlockColor.black(),
                            align: TextBlockAlign.auto)));
              });
            },
            iconData: widget.bloxDecoration.paragraphIcon,
            iconColor: widget.bloxDecoration.paragraphIconColor,
            iconSize: 18,
          ),

          /// Image
          AddBlockItem(
            onTap: () {
              String defaultImage = widget.imageEditor(null).defaultImageUrl;
              setState(() {
                display(false);
                widget.controller.bloxItems.insert(
                  index + 1,
                  ImageBlock(index + 1,
                      ImageBlockAttr(defaultImage, "Image", ""), defaultImage),
                );
              });
            },
            iconData: widget.bloxDecoration.imageIcon,
            iconColor: widget.bloxDecoration.imageIconColor,
            iconSize: 18,
          ),
        ];
      },
    );
  }

  _moveElement({oldIndex, newIndex}) {
    if (widget.controller.bloxItems.length > 0) {
      var oldItem = widget.controller.bloxItems[oldIndex];
      setState(() {
        widget.controller.bloxItems.removeAt(oldIndex);
        if (widget.controller.bloxItems.length < newIndex) {
          widget.controller.bloxItems.insert(newIndex - 1, oldItem);
        } else {
          widget.controller.bloxItems.insert(newIndex, oldItem);
        }
      });
    }
  }
}

class BloxController {
  List<Block> bloxItems = [];

  BloxController({List<Block> bloxItems}) {
    this.bloxItems = bloxItems ?? [];
  }
}

class BloxDecoration {
  final Widget noneBloxWidget;
  final String fieldsHint;
  final IconData chevronUpIcon,
      chevronDownIcon,
      deleteIcon,
      titleIcon,
      paragraphIcon,
      imageIcon,
      fontsDropdownMenuIcon,
      alignLeftIcon,
      alignCenterIcon,
      alignRightIcon,
      textSizeIcon;

  final Color chevronIconsColor,
      deleteIconColor,
      titleIconColor,
      textSizeIconColor,
      paragraphIconColor,
      imageIconColor,
      dropdownIconColor,
      alignActiveColor,
      alignDisabledColor;

  final Widget colorsDropdownMenuIcon;

  const BloxDecoration({
    this.noneBloxWidget,
    this.fieldsHint = "Type something...",
    this.chevronUpIcon = Icons.keyboard_arrow_up,
    this.chevronDownIcon = Icons.keyboard_arrow_down,
    this.deleteIcon = CombiIcons.trash_1,
    this.titleIcon = Icons.title,
    this.paragraphIcon = Icons.wrap_text,
    this.imageIcon = Icons.image,
    this.fontsDropdownMenuIcon = Icons.keyboard_arrow_down,
    this.colorsDropdownMenuIcon = const SizedBox.shrink(),
    this.alignLeftIcon = Icons.format_align_left,
    this.alignCenterIcon = Icons.format_align_center,
    this.alignRightIcon = Icons.format_align_right,
    this.textSizeIcon = Icons.title,
    this.chevronIconsColor = Colors.blue,
    this.deleteIconColor = Colors.red,
    this.titleIconColor = Colors.blue,
    this.textSizeIconColor = Colors.blue,
    this.paragraphIconColor = Colors.purple,
    this.imageIconColor = Colors.green,
    this.dropdownIconColor = Colors.blueGrey,
    this.alignActiveColor = Colors.blueGrey,
    this.alignDisabledColor = Colors.lightBlue,
  });
}
