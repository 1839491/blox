import 'dart:convert';
import 'dart:typed_data';

import 'package:blox/blox.dart';
import 'package:blox/models/block_document.dart';
import 'package:blox/widgets/blox_editor.dart';
import 'package:blox/widgets/blox_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Uint8List base64 = base64Decode(
      "eyJibG9ja3MiOlt7ImlkIjoyLCJ0eXBlIjoyLCJpbWFnZV91cmwiOiJodHRwOi8vdXVwbG9hZC5pci9maWxlcy9kZGU0X2Jsb3gucG5nIiwic3R5bGUiOnsicGxhY2Vob2xkZXIiOiJodHRwOi8vdXVwbG9hZC5pci9maWxlcy9kZGU0X2Jsb3gucG5nIiwiYWx0IjoiSW1hZ2UiLCJoeXBlcmxpbmsiOiIifX0seyJpZCI6MCwidHlwZSI6MSwidGV4dCI6IkJsb3gg4oCOUGx1Z2luIiwic3R5bGUiOnsiZm9udF9mYW1pbHkiOiJBbnRvbiIsImZvbnRfc2l6ZSI6NDIsIm11bHRpbGluZSI6ZmFsc2UsImFsaWduIjowLCJjb2xvciI6eyJyZWQiOjEzLCJncmVlbiI6NzEsImJsdWUiOjE2MSwiYWxwaGEiOjI1NX19fSx7ImlkIjoxLCJ0eXBlIjoxLCJ0ZXh0IjoiVXNlIGl0IG5vdywgeW91IGNhbiBmaW5kIGl0IGluIHB1Yi5kZXYgd2l0aCB0aGUgYmxveCBwYWNrYWdlbmFtZVxub3Igc2VhcmNoIHRoZSBiZWxvdyBsaW5rOlxucHViLmRldi9wYWNrYWdlcy9ibG94XG5IZXJlIHNvbWUgaWRlYSA6KSIsInN0eWxlIjp7ImZvbnRfZmFtaWx5IjoiUmFsZVdheSIsImZvbnRfc2l6ZSI6MTYsIm11bHRpbGluZSI6dHJ1ZSwiYWxpZ24iOi0xLCJjb2xvciI6eyJyZWQiOjAsImdyZWVuIjoxNTEsImJsdWUiOjE2NywiYWxwaGEiOjI1NX19fV19");
  Map<String, dynamic> json;
  BloxController controller;
  bool viewMode = false;

  @override
  void initState() {
    super.initState();
    json = jsonDecode(utf8.decode(base64));
    controller = BloxController(bloxItems: BloxDocument.fromJson(json).blocks);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Plugin example app'),
        ),
        backgroundColor: Colors.grey[200],
        body: Container(
            padding: EdgeInsets.all(20),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      children: [
                        RaisedButton(
                          onPressed: () {
                            print(base64Encode(utf8.encode(jsonEncode(
                                BloxDocument(controller.bloxItems).toJson()))));
                          },
                          child: Text(
                            "convert to json",
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        RaisedButton(
                          onPressed: () {
                            setState(() {
                              viewMode = !viewMode;
                            });
                          },
                          child: Text(
                            "View Mode/Editor Mode",
                          ),
                        ),
                      ],
                    ),
                    Visibility(
                      visible: viewMode,
                      child: Expanded(
                        child: BloxView(
                          onBlockTap: (block) {
                            print(block);
                          },
                          bloxDocument: BloxDocument(controller.bloxItems),
                        ),
                      ),
                    ),
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child: Visibility(
                        visible: !viewMode,
                        child: Expanded(
                          child: BloxEditor(
                            colorPickerHintLang: "FA",
                            controller: controller,
                            addButtonOpenOnHover: false,
                            canDeleteBlocks: true,
                            imageEditor: (imageBlock) {
                              return BloxImageEditor(
                                imageBlock: imageBlock,
                                defaultImageUrl:
                                    'http://uupload.ir/files/dde4_blox.png',
                                onImageSelect: (blob) {
                                  /// Upload image here and return image url
                                  return blob;
                                },
                                child: (onTap, message) {
                                  return Column(
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          onTap: onTap,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(25),
                                                border: Border.all(
                                                    color: Colors.blue,
                                                    style: BorderStyle.solid,
                                                    width: 2.5)),
                                            padding: EdgeInsets.symmetric(
                                                vertical: 8, horizontal: 16),
                                            child: Text(
                                              "انتخاب فایل",
                                              style: TextStyle(
                                                  fontFamily: "IranSans",
                                                  package: "blox"),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ))),
      ),
    );
  }
}
