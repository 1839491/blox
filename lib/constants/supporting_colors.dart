import 'package:blox/constants/package_config.dart';
import 'package:blox/widgets/blocks/text_editor.dart';
import 'package:flutter/material.dart';

class BloxColors {
  String colorsWithPersianHint = 'FA';
  String colorsWithEnglishHint = 'EN';

  /// Key(Color Hex): Value(name of the color)
  Map<String, dynamic> colorsMapFa = {
    "#000000": "مشکی",
    "#ef5350": "قرمز",
    "#d32f2f": "قرمز",
    "#c62828": "قرمز",
    "#c2185b": "صورتی",
    "#ad1457": "صورتی",
    "#880e4f": "صورتی",
    "#7b1fa2": "بنفش",
    "#6a1b9a": "بنفش",
    "#4a148c": "بنفش",
    "#2196f3": "آبی",
    "#1e88e5": "آبی",
    "#1976d2": "آبی",
    "#1565c0": "آبی",
    "#0d47a1": "آبی",
    "#0277bd": "آبی",
    "#01579b": "آبی",
    "#00bcd4": "فیروزه ای",
    "#00acc1": "فیروزه ای",
    "#0097a7": "فیروزه ای",
    "#00838f": "فیروزه ای",
    "#006064": "فیروزه ای",
    "#43a047": "سبز",
    "#388e3c": "سبز",
    "#2e7d32": "سبز",
    "#afb42b": "لیمویی",
    "#f57c00": "نارنجی",
    "#ef6c00": "نارنجی",
    "#e65100": "نارنجی",
    "#f4511e": "نارنجی",
    "#5d4037": "قهوه ای",
    "#d7ccc8": "نسکافه ای",
    "#bcaaa4": "نسکافه ای",
    "#a1887f": "نسکافه ای",
    "#424242": "طوسی",
    "#212121": "طوسی",
    "#ffee58": "زرد",
    "#ffeb3b": "زرد",
    "#ffea00": "زرد",
  };
  Map<String, dynamic> colorsMapEn = {
    "#000000": "Black",
    "#ef5350": "Red",
    "#d32f2f": "Red",
    "#c62828": "Red",
    "#c2185b": "Pink",
    "#ad1457": "Pink",
    "#880e4f": "Pink",
    "#7b1fa2": "Purple",
    "#6a1b9a": "Purple",
    "#4a148c": "Purple",
    "#2196f3": "Blue",
    "#1e88e5": "Blue",
    "#1976d2": "Blue",
    "#1565c0": "Blue",
    "#0d47a1": "Blue",
    "#0277bd": "Blue",
    "#01579b": "Blue",
    "#00bcd4": "Cyan",
    "#00acc1": "Cyan",
    "#0097a7": "Cyan",
    "#00838f": "Cyan",
    "#006064": "Cyan",
    "#43a047": "Green",
    "#388e3c": "Green",
    "#2e7d32": "Green",
    "#afb42b": "Lime",
    "#f57c00": "Orange",
    "#ef6c00": "Orange",
    "#e65100": "Orange",
    "#f4511e": "Orange",
    "#5d4037": "Brown",
    "#d7ccc8": "Brown",
    "#bcaaa4": "Brown",
    "#a1887f": "Brown",
    "#424242": "Grey",
    "#212121": "Grey",
    "#ffee58": "Yellow",
    "#ffeb3b": "Yellow",
    "#ffea00": "Yellow",
  };

//  /// Key(Color Hex): Value(name of the color)
//  Map<String, dynamic> colorsMap = {
//    "#000000": "مشکی",
//    "#0033CC": "آبی پررنگ",
//    "#428BCA": "آبی آسمانی",
//    "#44AD8E": "سبز لیمویی",
//    "#A8D695": "سبز فسفری",
//    "#5CB85C": "سبز متعادل",
//    "#69D100": "سبز روشن",
//    "#004E00": "سبز پررنگ",
//    "#34495E": "آبی سایه",
//    "#7F8C8D": "طوسی",
//    "#A295D6": "بنفش کمرنگ",
//    "#5843AD": "بنفش پررنگ",
//    "#8E44AD": "بنفش متعادل",
//    "#FFECDB": "کِرمی",
//    "#AD4363": "صورتی متعادل",
//    "#D10069": "صورتی",
//    "#CC0033": "قرمز متعادل",
//    "#FF0000": "قرمز",
//    "#D9534F": "قرمز ملایم",
//    "#D1D100": "زرد فسفری",
//    "#F0AD4E": "نارنجی فسفری",
//    "#AD8D43": "نسکافه ای",
//  };

  List<DropdownMenuItem<String>> getDropDownMenuItems({hintLang = "FA"}) {
    List<DropdownMenuItem<String>> items = List<DropdownMenuItem<String>>();
    if (hintLang == "FA") {
      colorsMapFa.forEach((key, value) {
        items.add(dropdownItem(key, value));
      });
    } else {
      colorsMapEn.forEach((key, value) {
        items.add(dropdownItem(key, value));
      });
    }
    return items;
  }

  DropdownMenuItem<String> dropdownItem(key, value) {
    return DropdownMenuItem(
        value: key.toLowerCase(),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: 40,
                height: 10,
                decoration: BoxDecoration(
                  color: hexToColor(key),
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              Text(
                " Aa ",
                style: TextStyle(
                  color: hexToColor(key),
                ),
              )
            ],
          ),
        ));
  }

  DropdownMenuItem<String> dropdownItemWithTitle(key, value) {
    return DropdownMenuItem(
        value: key.toLowerCase(),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Row(
            children: [
              Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                    color: hexToColor(key),
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          spreadRadius: 0.5,
                          blurRadius: 10),
                    ]),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                value,
                textDirection:
                    isRTL(value) ? TextDirection.rtl : TextDirection.ltr,
                textAlign: isRTL(value) ? TextAlign.right : TextAlign.left,
                softWrap: true,
                style: TextStyle(
                    fontFamily: "IranSans",
                    package: PackageConfig.PACKAGE_NAME),
              ),
            ],
          ),
        ));
  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  String colorToHex(Color color) {
    return "#" + color.value.toRadixString(16).replaceFirst("ff", "");
  }
}
