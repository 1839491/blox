import 'package:json_annotation/json_annotation.dart';

part 'color_block.g.dart';

@JsonSerializable()
class BlockColor {
  final int red;
  final int green;
  final int blue;
  final int alpha;

  const BlockColor.black(
      {this.red = 0, this.green = 0, this.blue = 0, this.alpha = 255});
  const BlockColor.white(
      {this.red = 255, this.green = 255, this.blue = 255, this.alpha = 255});

  const BlockColor(this.red, this.green, this.blue, this.alpha);

  factory BlockColor.fromJson(Map<String, dynamic> json) =>
      _$BlockColorFromJson(json);

  Map<String, dynamic> toJson() => _$BlockColorToJson(this);
}
