import 'package:blox/models/block.dart';
import 'package:blox/models/video_block_attr.dart';
import 'package:json_annotation/json_annotation.dart';

part 'video_block.g.dart';

/// VideoBlock model.
@JsonSerializable()
class VideoBlock extends Block {
  @JsonKey(defaultValue: -1)
  final int id;
  @JsonKey(defaultValue: BlockType.undefined)
  final BlockType type;
  @JsonKey(name: 'video_url')
  String videoUrl;
  final VideoBlockAttr style;

  VideoBlock(this.id, this.videoUrl, this.style, {this.type = BlockType.video})
      : super(id, BlockType.video);

  factory VideoBlock.fromJson(Map<String, dynamic> json) =>
      _$VideoBlockFromJson(json);

  Map<String, dynamic> toJson() => _$VideoBlockToJson(this);
}
