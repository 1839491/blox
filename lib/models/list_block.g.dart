// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_block.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListBlock _$ListBlockFromJson(Map<String, dynamic> json) {
  return ListBlock(
    json['id'] as int ?? -1,
    (json['items'] as List)?.map((e) => e as String)?.toList(),
    type: _$enumDecodeNullable(_$BlockTypeEnumMap, json['type']) ??
        BlockType.undefined,
  );
}

Map<String, dynamic> _$ListBlockToJson(ListBlock instance) => <String, dynamic>{
      'id': instance.id,
      'type': _$BlockTypeEnumMap[instance.type],
      'items': instance.items,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$BlockTypeEnumMap = {
  BlockType.text: 1,
  BlockType.image: 2,
  BlockType.video: 3,
  BlockType.list: 4,
  BlockType.undefined: -1,
};
