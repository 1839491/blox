import 'package:blox/models/block.dart';
import 'package:json_annotation/json_annotation.dart';

import 'image_block_attr.dart';

part 'image_block.g.dart';

/// ImageBlock model.
@JsonSerializable()
class ImageBlock extends Block {
  @JsonKey(defaultValue: -1)
  final int id;
  @JsonKey(defaultValue: BlockType.undefined)
  final BlockType type;
  @JsonKey(name: 'image_url')
  String imageUrl;
  final ImageBlockAttr style;

  ImageBlock(this.id, this.style, this.imageUrl, {this.type = BlockType.image})
      : super(id, BlockType.image);

  factory ImageBlock.fromJson(Map<String, dynamic> json) =>
      _$ImageBlockFromJson(json);

  Map<String, dynamic> toJson() => _$ImageBlockToJson(this);
}
