// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_block_attr.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageBlockAttr _$ImageBlockAttrFromJson(Map<String, dynamic> json) {
  return ImageBlockAttr(
    json['placeholder'] as String,
    json['alt'] as String,
    json['hyperlink'] as String,
  );
}

Map<String, dynamic> _$ImageBlockAttrToJson(ImageBlockAttr instance) =>
    <String, dynamic>{
      'placeholder': instance.placeholder,
      'alt': instance.altText,
      'hyperlink': instance.hyperlink,
    };
