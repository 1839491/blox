import 'package:blox/models/block.dart';
import 'package:json_annotation/json_annotation.dart';

part 'list_block.g.dart';

/// ListBlock model.
@JsonSerializable()
class ListBlock extends Block {
  @JsonKey(defaultValue: -1)
  final int id;
  @JsonKey(defaultValue: BlockType.undefined)
  final BlockType type;
  List<String> items;

  ListBlock(this.id, this.items, {this.type = BlockType.list})
      : super(id, BlockType.list);

  factory ListBlock.fromJson(Map<String, dynamic> json) =>
      _$ListBlockFromJson(json);

  Map<String, dynamic> toJson() => _$ListBlockToJson(this);
}
