import 'package:blox/constants/constants.dart';
import 'package:blox/models/color_block.dart';
import 'package:json_annotation/json_annotation.dart';

part 'text_block_attr.g.dart';

/// TextBlock Attributes (styles) model.
@JsonSerializable()
class TextBlockAttr {
  @JsonKey(
      name: 'font_family',
      defaultValue: Constants.DEFAULT_FONT_FAMILY,
      required: false)
  String fontFamily;
  @JsonKey(
      name: 'font_size',
      defaultValue: Constants.DEFAULT_FONT_SIZE,
      required: false)
  double fontSize;
  @JsonKey(defaultValue: false, required: false)
  bool multiline;
  @JsonKey(defaultValue: TextBlockAlign.auto, required: false)
  TextBlockAlign align;
  @JsonKey(required: false, toJson: _colorToJson)
  BlockColor color;

//  @JsonKey(name: 'font_weight')
//  final TextBlockFontWeight fontWeight;

  TextBlockAttr(this.fontFamily, this.fontSize, this.multiline,
      {this.align = TextBlockAlign.auto,
      this.color = const BlockColor.black()});

  factory TextBlockAttr.fromJson(Map<String, dynamic> json) =>
      _$TextBlockAttrFromJson(json);

  Map<String, dynamic> toJson() => _$TextBlockAttrToJson(this);
}

enum TextBlockAlign {
  @JsonValue(-1)
  auto,
  @JsonValue(0)
  center,
  @JsonValue(1)
  left,
  @JsonValue(2)
  right,
}

_colorToJson(color) {
  return color.toJson();
}
//enum TextBlockFontWeight {
//  @JsonValue(100)
//  W100,
//  @JsonValue(200)
//  W200,
//}
