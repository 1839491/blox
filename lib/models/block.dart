import 'package:json_annotation/json_annotation.dart';

/// Simple abstract Block class.
abstract class Block {
  final int id;
  final BlockType type;

  Block(this.id, this.type);
}

enum BlockType {
  @JsonValue(1)
  text,
  @JsonValue(2)
  image,
  @JsonValue(3)
  video,
  @JsonValue(4)
  list,
  @JsonValue(-1)
  undefined,
}
